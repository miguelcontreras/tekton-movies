require 'rails_helper'

describe SalesForceService do
  let(:service) {described_class.new}

  it "should generate token for SalesForce" do
    expect(service.generate_token).to_not be_nil
  end
end