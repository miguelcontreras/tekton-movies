// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery-ui
//= require jquery_ujs
//= require libraries/metisMenu
//= require libraries/fontawesome-markers.min
//= require theme
//= require bootstrap-datepicker
//= require bootstrap-datepicker/locales/bootstrap-datepicker.es
//= require moment
//= require moment/es
//= require bootstrap-datetimepicker
//= require select2
//= require select2_locale_es
//= require libraries/bootstrap-clockpicker.min

$(document).ready(tekton);

function tekton() {
    moment().format('es');

    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        language: 'es'
    });

    $('.datetimepicker').datetimepicker();

    $("select").select2({
        theme: "bootstrap",
        locale: 'es'
    });

    $.formUtils.addValidator({
        name: 'datetime',
        validatorFunction: function(value, $el, config, language, $form) {
            console.log(value);

            if(value == "" || value == undefined || value == null){
                return false;
            }

            var stamp = value.split(" ");

            var date = stamp[0];
            var day = parseInt(date.split("/")[0]);
            var month = parseInt(date.split("/")[1]);
            var year = parseInt(date.split("/")[2]);

            var time = stamp[1];
            var hour = parseInt(time.split(":")[0]);
            var minutes = parseInt(time.split(":")[1]);

            var validDate = (day > 0 && day < 32 && month > 0 && month < 13 && year > 2000);
            var validTime = (hour >= 0 && hour < 25 && minutes >= 0 && minutes < 61);

            return (validDate && validTime);
        },
        errorMessage: 'La fecha y hora proporcionada no es válida.',
        errorMessageKey: 'invalidDateTime'
    });

    $.validate({
        lang: 'es',
        modules: 'date'
    });

    $.fn.dataTable.moment('DD/MM/YYYY');
    $.fn.dataTable.moment('DD/MM/YYYY HH:mm');

    $("table:not(.datatable-with-paging)").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "order": []
    });

    $("table.datatable-with-paging").DataTable({
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        order: [],
        paging: false,
        info: false,
        searching: false
    });

    $.rails.allowAction = function(link){
        if (link.data("confirm") == undefined){
            return true;
        }
        $.rails.showConfirmationDialog(link);
        return false;
    };

    $.rails.confirmed = function(link){
        link.data("confirm", null);
        link.trigger("click.rails");
    };

    $.rails.showConfirmationDialog = function(link){
        var message = link.data("confirm");
        swal({
            title: 'Estás Seguro?',
            text: message,
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        }).then(function(confirm){
            if(confirm){
                $.rails.confirmed(link);
            }
        });
    };
}
