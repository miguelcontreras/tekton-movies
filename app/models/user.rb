class User < ApplicationRecord
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable

  # Uniqueness
  validates :email, presence: true
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, message: "ingresa un e-mail válido."
  validate :email_should_be_unique

  def full_name
    "#{first_name} #{last_name}"
  end

  def email_should_be_unique
    if self.new_record?
      if email.present? and not User.where(email: email).empty?
        errors.add(:email, "ya se encuentra registrado.")
      end
    end
  end
end
