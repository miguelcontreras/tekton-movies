class Movie < ApplicationRecord
  # Image Attached
  has_attached_file :image, styles: { large: "600x500>",medium: "300x300>", thumb: "100x100>" }, default_url: "/assets/movies/default.png"
  validates_attachment :image, content_type: { content_type: ["image/jpeg", "image/jpg", "image/gif", "image/png"] }

  belongs_to :category
  belongs_to :client, optional: true
end
