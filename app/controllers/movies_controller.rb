class MoviesController < ApplicationController
  before_action :set_movie, only: [:show, :edit, :update, :destroy,:return_movie, :rent, :update_rent]
  before_action :set_default_form, only: [:edit, :new]

  add_breadcrumb "Inicio", :root_path
  add_breadcrumb "Películas", :movies_path

  # GET /movies
  # GET /movies.json
  def index
    add_breadcrumb "Listado"

    @movies = Movie.includes(:client, :category).order(name: :asc).all
  end

  # GET /movies/1
  # GET /movies/1.json
  def show
    add_breadcrumb @movie.name
    add_breadcrumb "Detalles"
  end

  # GET /movies/new
  def new
    add_breadcrumb "Agregar"

    @movie = Movie.new
  end

  # GET /movies/1/edit
  def edit
    add_breadcrumb @movie.name
    add_breadcrumb "Editar"
  end

  # POST /movies
  # POST /movies.json
  def create
    @movie = Movie.new(movie_params)

    respond_to do |format|
      if @movie.save
        format.html { redirect_to @movie, notice: 'La película ha sido creada exitosamente.' }
        format.json { render :show, status: :created, location: @movie }
      else
        format.html { render :new }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /movies/1
  # PATCH/PUT /movies/1.json
  def update
    respond_to do |format|
      if @movie.update(movie_params)
        format.html { redirect_to @movie, notice: 'La película ha sido actualizada exitosamente.' }
        format.json { render :show, status: :ok, location: @movie }
      else
        format.html { render :edit }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /movies/1/rent
  def rent
    if @movie.client.nil?
      add_breadcrumb @movie.name
      add_breadcrumb "Rentar"

      @clients = Client.order(name: :asc)
    else
      respond_to do |format|
        format.html { redirect_to movies_url, notice: 'La Película ya estaba rentada.' }
        format.json { head :no_content }
      end
    end
  end

  # PUT /movies/1/rent
  def update_rent
    respond_to do |format|
      if @movie.client.nil?
        if @movie.update(client_id: movie_params[:client_id], rents: (@movie.rents + 1))
          @client = @movie.client

          @client.update(rents: (@client.rents + 1)) unless @client.nil?

          sales_force_service = SalesForceService.new
          sales_force_service.rented_movie(@movie)

          format.html { redirect_to @movie, notice: 'La película ha sido rentada exitosamente.' }
          format.json { render :show, status: :ok, location: @movie }
        else
          format.html { render :rent }
          format.json { render json: @movie.errors, status: :unprocessable_entity }
        end
      else
        format.html { redirect_to movies_url, notice: 'La Película ya estaba rentada.' }
        format.json { head :no_content }
      end
    end
  end

  def return_movie
    respond_to do |format|
      if @movie.update(client_id: nil)
        sales_force_service = SalesForceService.new
        sales_force_service.return_movie(@movie)

        format.html { redirect_to movies_url, notice: 'La película ha sido retornada exitosamente.' }
        format.json { head :no_content }
      else
        format.html { render :index }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /movies/1
  # DELETE /movies/1.json
  def destroy
    @movie.destroy
    respond_to do |format|
      format.html { redirect_to movies_url, notice: 'La película ha sido eliminada exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    def set_default_form
      @categories = Category.order(name: :asc)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_movie
      @movie = Movie.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def movie_params
      params.require(:movie).permit(:name, :director, :email, :release_date, :category_id, :image, :client_id)
    end
end
