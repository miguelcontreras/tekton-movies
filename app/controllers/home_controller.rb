class HomeController < ApplicationController
  add_breadcrumb "Inicio", :root_path

  def index
    @movies = Movie.order(rents: :desc).limit(4)
    @clients = Client.order(rents: :desc).limit(10)
  end
end
