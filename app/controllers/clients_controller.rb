class ClientsController < ApplicationController
  before_action :set_client, only: [:show, :edit, :update, :destroy]

  add_breadcrumb "Inicio", :root_path
  add_breadcrumb "Clientes", :clients_path

  # GET /clients
  # GET /clients.json
  def index
    @clients = Client.includes(:movies).order(name: :asc).all
  end

  # GET /clients/1
  # GET /clients/1.json
  def show
    add_breadcrumb @client.name
    add_breadcrumb "Detalles"
  end

  # GET /clients/new
  def new
    add_breadcrumb "Agregar"

    @client = Client.new
  end

  # GET /clients/1/edit
  def edit
    add_breadcrumb @client.name
    add_breadcrumb "Editar"
  end

  # POST /clients
  # POST /clients.json
  def create
    @client = Client.new(client_params)

    respond_to do |format|
      if @client.save
        format.html { redirect_to @client, notice: 'El Cliente ha sido creado exitosamente.' }
        format.json { render :show, status: :created, location: @client }
      else
        format.html { render :new }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /clients/1
  # PATCH/PUT /clients/1.json
  def update
    respond_to do |format|
      if @client.update(client_params)
        format.html { redirect_to @client, notice: 'El Cliente ha sido actualizado exitosamente.' }
        format.json { render :show, status: :ok, location: @client }
      else
        format.html { render :edit }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clients/1
  # DELETE /clients/1.json
  def destroy
    if @client.movies.count == 0
      @client.destroy
      respond_to do |format|
        format.html { redirect_to clients_url, notice: 'El Cliente ha sido eliminado exitosamente.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to client_url, notice: 'El Cliente tiene peliculas rentadas.' }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client
      @client = Client.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def client_params
      params.require(:client).permit(:name, :email)
    end
end
