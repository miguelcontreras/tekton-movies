json.extract! movie, :id, :name, :director, :category_id, :release_date, :user_id, :rents, :created_at, :updated_at
json.url movie_url(movie, format: :json)
