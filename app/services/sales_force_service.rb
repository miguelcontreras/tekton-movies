require 'httparty'
require 'webmock'

class SalesForceService
  include HTTParty, WebMock::API

  #Inicializando valores
  def initialize
    user = ENV['SALES_FORCE_USERNAME']
    pass = ENV['SALES_FORCE_PASSWORD']

    @url = ENV['SALES_FORCE_URL']
    @basic_token = Base64.strict_encode64(user+':'+pass).chomp

    WebMock.enable!

    stub_request(:post, @url + '/information/movies')
        .to_return { |request| {body: { result: 'OK',message: 'registered', id: rand(1000), data: JSON.parse(request.body) }.to_json, status: 200, headers: {content_type: 'application/json'}}}

    stub_request(:post, @url + '/generate-token')
        .with(basic_auth: [user, pass])
        .to_return(body: { result: 'OK', token: rand(36**8).to_s(36)}.to_json, status: 200, headers: {content_type: 'application/json'})

    @token = generate_token
  end

  def generate_token
    response = HTTParty.post(
        @url + '/generate-token',
        headers: {
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': 'Basic ' + @basic_token
        }
    )

    if response.code == 200 && response['token'].present?
      response['token']
    else
      nil
    end
  end

  def rented_movie(movie)
    self.call(movie, 'RENTED')
  end

  def return_movie(movie)
    self.call(movie, 'RETURNED')
  end

  #llamar al endpoint que actualiza la información de una película, si no se generó el token no llamará nada
  def call(movie, status)
    if @token.nil?
      false
    else
      response = HTTParty.post(
          @url + '/information/movies',
          body: {
              movie_id: movie.id,
              client_id: movie.client_id,
              status: status,
              date: DateTime.now.strftime("%Y-%m-%d %H:%I:%S").to_s
          }.to_json,
          headers: {
              'Content-Type': 'application/json; charset=UTF-8',
              'Authorization': 'Bearer ' + @token
          }
      )

      if response.code == 200
        response.body
      else
        false
      end
    end
  end
end
