class FormatSecondaryEmails
  def self.call(*args, &block)
    new(*args, &block).perform
  end

  def initialize(string)
    @emails = string
  end

  def perform
    @emails.split(';')
  end
end
