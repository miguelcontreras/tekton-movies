password = '123456'
new_hashed_password = User.new(:password => password).encrypted_password
User.create!(username: "admin", first_name: "Miguel", last_name: "Contreras", email: "migueljosecontreras@gmail.com", encrypted_password: new_hashed_password, created_at: DateTime.now.to_s, updated_at: DateTime.now.to_s)

action_category = Category.create!(id:1, name: "Acción", description: "Este tipo de película son de alta tensión y contienen persecuciones y muchas peleas, además de una dirección que pone énfasis en el movimiento. Incluyen rescates, batallas, escapadas, explosiones… Su ritmo es espectacular, donde el bueno o los buenos suelen combatir con los malos. ")
adventure_category = Category.create!(id:2, name: "Aventura", description: "Estas películas cuentan historias interesantes y excitantes en contextos normalmente exóticos, y con un contenido similar al de las películas de acción. Suelen ocurrir en el desierto, la jungla o en el mar. Los personajes, generalmente, van en busca de un tesoro.")
comedy_category = Category.create!(id:3, name: "Comedia", description: "Son películas divertidas, construidas para que el espectador pase un rato divertido y no deje de reír. Suele ocurrir en multitud de contextos, pero, a diferencia de las películas normales, las comedias exageran la situación para que el público se ría a carcajadas.")
drama_category = Category.create!(id:4, name: "Dramáticas", description: "Son películas serias, con personajes y situaciones muy realistas, parecidas a la vida cotidiana, que incluyen situaciones tensas y dramáticas, y que pueden acabar mal o no")
terror_category = Category.create!(id:5, name: "Terror", description: "Las películas de terror pretenden despertar nuestro miedo con escenas chocantes, tensas y terroríficas, o bien mediante una ambientación y una dirección que producen angustia. Las películas de terror pueden incluir personajes poco realistas, como muertos vivientes o seres no humanos.")

client = Client.create!(name: "Juan García", email: "juan.garcia@tektonlabs.com", rents: 1)
Client.create!(name: "Brando Chirinos", email: "brando.chirinos@tektonlabs.com")

Movie.create!(id: '1',name: "Duro de Matar", director: "John McTiernan", category: action_category, client: client, release_date: '1988-02-24', rents: 1, image_file_name: '1.png', image_content_type: 'image/png', image_file_size: '294340')
Movie.create!(id: '2',name: "Arma Letal", director: "Richard Donner", category: action_category, release_date: '1987-02-24', image_file_name: '2.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '3',name: "Matrix", director: "Lana & Lily Wachowski", category: action_category, release_date: '1999-02-24', image_file_name: '3.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '4',name: "John Wick", director: "Chad Stahelski & David Leitch", category: action_category, release_date: '2014-02-24', image_file_name: '4.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '5',name: "Kill Bill", director: "Quentin Tarantino", category: action_category, release_date: '2007-02-24', image_file_name: '5.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '6',name: "La vieja guardia", director: "Quentin Tarantino", category: action_category, release_date: '2020-02-24', image_file_name: '6.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')

Movie.create!(id: '7',name: "Indiana Jones y el templo maldito", director: "Steven Spielberg", category: adventure_category, release_date: '1984-02-24', image_file_name: '7.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '8',name: "Jurassic Park", director: "Steven Spielberg", category: adventure_category, release_date: '1993-02-24', image_file_name: '8.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '9',name: "Piratas del Caribe: la maldición de la Perla Negra", director: "Gore Verbinski", category: adventure_category, release_date: '2003-02-24', image_file_name: '9.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '10',name: "King Kong", director: "Peter Jackson", category: adventure_category, release_date: '2005-02-24', image_file_name: '10.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '11',name: "Apocalypto", director: "Mel Gibson", category: adventure_category, release_date: '2006-02-24', image_file_name: '11.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')

Movie.create!(id: '12',name: "Focus", director: "Glenn Ficarra, John Requa", category: comedy_category, release_date: '2015-02-24', image_file_name: '12.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '13',name: "Dick and Jane", director: "Dean Parisot", category: comedy_category, release_date: '2005-02-24', image_file_name: '13.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '14',name: "Deadpool", director: "Tim Miller", category: comedy_category, release_date: '2016-02-24', image_file_name: '14.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '15',name: "La máscara", director: "Chuck Russell", category: comedy_category, release_date: '1994-02-24', image_file_name: '15.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '16',name: "Los padres de ella", director: "Jay Roach", category: comedy_category, release_date: '2000-02-24', image_file_name: '16.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')

Movie.create!(id: '17',name: "El niño con el pijama de rayas", director: "Mark Herman", category: drama_category, release_date: '2008-02-24', image_file_name: '17.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '18',name: "El pianista", director: "Roman Polanski", category: drama_category, release_date: '2002-02-24', image_file_name: '18.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '19',name: "Mi nombre es Khan", director: "Karan Johar", category: drama_category, release_date: '2010-02-24', image_file_name: '19.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '20',name: "Hasta el último hombre", director: "Mel Gibson", category: drama_category, release_date: '2016-02-24', image_file_name: '20.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '21',name: "En busca de la felicidad", director: "Gabriele Muccino", category: drama_category, release_date: '2007-02-24', image_file_name: '21.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')

Movie.create!(id: '22',name: "Psycho (Psicosis)", director: "Gus Van Sant", category: terror_category, release_date: '1999-02-24', image_file_name: '22.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '23',name: "Annabelle: Creation", director: "David F. Sandberg", category: terror_category, release_date: '2017-02-24', image_file_name: '23.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '24',name: "The Conjuring", director: "James Wan", category: terror_category, release_date: '2013-02-24', image_file_name: '24.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '25',name: "Drácula de Bram Stoker", director: "Francis Ford Coppola", category: terror_category, release_date: '1993-02-24', image_file_name: '25.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')
Movie.create!(id: '26',name: "El Exorcista", director: "William Friedkin", category: terror_category, release_date: '1975-02-24', image_file_name: '26.jpg', image_content_type: 'image/jpeg', image_file_size: '294340')


