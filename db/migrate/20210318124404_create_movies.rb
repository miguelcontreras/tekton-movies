class CreateMovies < ActiveRecord::Migration[5.0]
  def change
    create_table :movies do |t|
      t.string :name
      t.string :director
      t.references :category, foreign_key: true, null: false
      t.date :release_date
      t.references :client, foreign_key: true, null: true
      t.integer :rents, null: false, default: 0
      t.attachment :image

      t.timestamps
    end
  end
end
