# Tekton Movies

### Ruby version
```
2.5.0p0
```

### Bundle version
```
1.17.3
```

### Start System
Generate .env file and replace it with your data
```
cp .env.sample ./.env
```

Install Gems
```
bundle install
```

### System dependencies
  All dependencies are listed in Gemfile, if you have problems with pg or uploading images, you need the libraries
```
sudo apt-get install libpq-dev
sudo apt-get install imagemagick
```

### Database creation

Execute this commands for generate the database

```
bundle exec rake db:create
bundle exec rake db:migrate
bundle exec rake db:seed
bundle exec rake db:migrate RAILS_ENV=test
bundle exec rake db:seed RAILS_ENV=test
```

Execute this tests to verify that is all good

```
bundle exec rails test test/
bundle exec rspec spec/sales_force.rb
```


### Starting server
Development enviromnet
to start, use:
```
bundle exec rails s
```
to stop, use:
```
ctrl + c
```
  
### System auth

```
admin
123456
```