server 'tekton.migueljosecontreras.com', user: 'mcontreras', roles: [:web, :app, :db]
set :puma_threads,    [1, 4]
set :puma_workers,    0
set :puma_init_active_record, true
set :puma_bind, %w(tcp://0.0.0.0:9293)
set :stage,           :production
set :branch,          :develop
set :deploy_to,       '/home/tekton/rails/'
