require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

Dotenv::Railtie.load

module TektonMovies
  class Application < Rails::Application
    # I18n settings
    config.i18n.default_locale = :es

    # Datetime Settings
    # Date
    Date::DATE_FORMATS[:default] = "%d/%m/%Y"

    # Time
    Time::DATE_FORMATS[:default] = "%d/%m/%Y %H:%M"

    config.time_zone = 'America/Caracas'
    config.active_record.default_timezone = :local

    # Default URL for Mails
    config.action_mailer.default_url_options = { host: "localhost" }

    # Enable rails to serve my assets
    config.serve_static_assets = true
  end
end
