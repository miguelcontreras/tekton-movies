Rails.application.routes.draw do
  root 'home#index'

  # Users
  devise_for :users, controllers: { passwords: "passwords" }, skip: :registrations
  resources :users do
    get 'enable', on: :member
    get 'disable', on: :member
    get 'deleted', on: :collection
    get 'restore', on: :member
  end

  resources :clients

  resources :movies do
    post 'return', action: :return_movie, on: :member
    get 'rent', on: :member
    put 'rent', action: :update_rent, on: :member
  end
end
