set :repo_url,        'miguelcontreras@bitbucket.org:miguelcontreras/tekton-movies.git'
set :application,     'tekton-movies'
lock "1"

set :rvm_ruby_version, '2.5.0'

set :pty,             false
set :use_sudo,        false
set :deploy_via,      :copy
# set :deploy_to,       "/home/#{fetch(:application)}/rails/"
set :ssh_options,     { forward_agent: true }
set :log_level, :info

set :linked_dirs,  %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}
set :linked_files, %w{.env}
set :sidekiq_queue, %w{default mailers}

namespace :deploy do
  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      invoke 'deploy'
    end
  end

  after  :finishing,    :compile_assets
  after  :finishing,    :cleanup
end
