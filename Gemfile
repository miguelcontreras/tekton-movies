source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '~> 5.0.2'
gem 'pg', '~> 0.18'
gem 'httparty', '~> 0.18'
gem 'puma', '~> 3.0'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'therubyracer', platforms: :ruby
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'devise'
gem 'devise-i18n'
gem 'cancancan', '~> 1.10'
gem 'i18n_generators'

gem 'jbuilder', '~> 2.5'
gem 'paperclip', '5.2.1'

gem 'select2-rails'
gem 'bootstrap-datepicker-rails'

# DateTimePicker
gem 'momentjs-rails', '>= 2.9.0'
gem 'bootstrap3-datetimepicker-rails', '~> 4.17.47'

# Spreadsheeting
gem 'axlsx'
gem 'roo'
gem 'spreadsheet'
gem 'rubyzip'
gem 'axlsx_rails'

# Asynchronous work
gem 'sidekiq'
gem 'sinatra', '>= 1.3.0', require: nil
gem "daemons"

# Breadcrumbs
gem "breadcrumbs_on_rails"

# Time Differences
gem "time_diff"

# Pagination
gem "will_paginate"
gem "will_paginate-bootstrap"

# Deployment
group :development do
  gem 'capistrano', '3.8.1', require: false
  gem 'capistrano-rbenv', require: false
  gem 'capistrano-rvm', require: false
  gem 'capistrano-rails', require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano-passenger', require: false
  gem 'capistrano-sidekiq', require: false
  gem 'capistrano-faster-assets', '~> 1.0', require: false
  gem 'capistrano-rails-console', require: false
  gem 'capistrano3-puma',   require: false
end

group :development do
  gem 'rails_real_favicon'
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
  gem 'pry-rails'
  gem 'rspec-rails', '~> 3.5'
  gem 'webmock'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'dotenv-rails'
